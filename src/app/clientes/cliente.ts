export class Cliente {

    id: number;
    name: string;
    last_name: string;
    email: string;
    password: string;
    age: number;
    city: string;
    photo: string;
    sex: number;
    desc: string;

}
