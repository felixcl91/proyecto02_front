import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directiva',
  templateUrl: './directiva.component.html',
  styleUrls: ['./directiva.component.css']
})
export class DirectivaComponent implements OnInit {

  listaCurso: string[] = ['TypeScript', 'Node.js', 'Angular', 'Spring'];

  //boton que inicializamos en true para que se vea el listado
  habilitar: boolean=true;

  constructor() { }

  //metodo que muestra o no un contenido con la condicion if
  setHabilitar(): void{
    this.habilitar = (this.habilitar == true)? false: true;
  }

  ngOnInit(): void {
  }

}
